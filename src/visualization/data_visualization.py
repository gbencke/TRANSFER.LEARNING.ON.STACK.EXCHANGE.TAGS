def create_web_page_header():
    return """
            <!DOCTYPE html>
            <html lang="en">
            <head>
              <title>Data Visualization</title>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            </head>
            <body>
            """


def create_web_page_footer():
    return """
            </body>
            </html>
            """


def create_initial_data_visualization(data_to_write, start_index, end_index):
    section_header = """
                    <div class="container">
                      <h2>Initial Data Samples</h2>
                      <p>Data as seen on the dictionary:</p>
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>File</th>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th>Tags</th>
                          </tr>
                        </thead>
                        <tbody>
                     """
    section_main = ""
    for sentence in data_to_write[start_index:end_index]:
        section_main += """
                                      <tr>
                                         <td> %s </td>
                                         <td> %s </td>
                                         <td> %s </td>
                                         <td> %s </td>
                                         <td> %s </td>
                                      </tr>
                                      """ % (
            sentence["file"], sentence["id"], sentence["title"], sentence["content"], sentence["tags"])

    section_footer = """
                         </tbody>
                       </table>
                     </div>
                     """
    return section_header + section_main + section_footer


def create_tag_view_main(tag_list):
    return ""


def create_initial_data(file_to_write, tree_to_show):
    f = open(file_to_write, "w", encoding="utf-8")
    f.write(
        create_web_page_header() + create_initial_data_visualization(tree_to_show, 0, 100) + create_web_page_footer())
    f.close()


def create_tag_view(file_to_write, tag_list):
    f = open(file_to_write, "w", encoding="utf-8")
    f.write(create_web_page_header() + create_tag_view_main(tag_list) + create_web_page_footer())
    f.close()


def create_cases_view_main(cases, f):
    f.write("<table>")
    for current_case in cases:
        f.write(
            "<tr style='border-top: 3px solid black;font-weight:bold'><td>Id:{}</td></tr>".format(
                current_case['id']))
        f.write("<tr style='font-weight:bold'><td>{}</td></tr>".format(current_case['file']))
        f.write("<tr><td>{}</td></tr>".format(current_case['content']))
        f.write("<tr><td>Nouns:{}</td></tr>".format(current_case['nouns']))
        f.write("<tr><td>Keywords:{}</td></tr>".format(current_case['keywords']))
        f.write("<tr style='border-bottom: 3px solid black;'><td>Tags:{}</td></tr>".format(
            current_case['taglist'] if 'taglist' in current_case else ""))
    f.write("</table>")


def create_cases_view(file_to_write, cases):
    f = open(file_to_write, "w", encoding="utf-8")
    f.write(create_web_page_header())
    create_cases_view_main(cases, f)
    f.write(create_web_page_footer())
    f.close()
