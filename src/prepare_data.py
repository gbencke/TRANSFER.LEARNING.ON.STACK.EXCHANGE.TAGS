from os import listdir
from os.path import join
import cache.cache
from cache.pickle import *
from nltk import word_tokenize, pos_tag
from nltk.stem.porter import PorterStemmer
from helpers import represents_int, remove_html_tags


def return_tokens_from_file(file_to_read):
    f = open(file_to_read, "r", encoding="UTF-8")
    total_string = f.read()
    total_string = total_string.replace(',""', ',\"').replace('"\n"', '","')
    tokens_string = total_string.split('","')
    return tokens_string


def parse_structure(file_to_read, tokens_per_line):
    cases_to_return = []
    tokens_string = return_tokens_from_file(file_to_read)
    number_of_lines = int(len(tokens_string) / tokens_per_line)
    removed_headers = tokens_string[tokens_per_line:(number_of_lines - 1) * tokens_per_line]
    for current_line in range(number_of_lines - 2):
        current_case = {
            "file": file_to_read,
            "id": removed_headers[current_line * tokens_per_line],
            "title": removed_headers[current_line * tokens_per_line + 1],
            "content": removed_headers[current_line * tokens_per_line + 2],
            "tags": removed_headers[current_line * tokens_per_line + 3] if tokens_per_line > 3 else ""
        }
        if not represents_int(current_case["id"]):
            raise TypeError("Unexpected string sequence for the id field of the csv file:" + file_to_read)
        cases_to_return.append(current_case)
    return cases_to_return


def extract_tokens(text_to_parse):
    nouns = []
    text = word_tokenize(text_to_parse)
    tokens_parsed = pos_tag(text)
    stemmer = PorterStemmer()
    for token in tokens_parsed:
        if len(token[0]) > 4 and (token[1] == 'NN' or token[1] == 'NNS'):
            nouns.append(stemmer.stem(token[0]))
    return list(set(nouns))


def analyse_structure(tree_to_analyse):
    for sentence in tree_to_analyse:
        sentence["taglist"] = sentence["tags"].split(" ") if len(sentence["tags"]) > 0 else []
        sentence["stripped_text"] = remove_html_tags(sentence["title"] + ' ' + sentence["content"]).replace("\n", "")
        sentence["nouns"] = list(set(extract_tokens(sentence["stripped_text"])))
        print("Parsing Case:" + sentence["id"])
    return tree_to_analyse


def get_data_structure(settings):
    if pickle_file_is_available("train.pickle") and pickle_file_is_available("test.pickle"):
        if pickle_file_is_available("tag_list.pickle"):
            train_cases = read_pickle("train.pickle")
            test_cases = read_pickle("test.pickle")
            tags = read_pickle("tag_list.pickle")
            return train_cases, tags, test_cases
    if cache.cache.is_cache_information_available(settings):
        return cache.cache.get_data_structure_from_cache(settings)
    return create_data_structure(settings)


def get_tests_results(settings):
    return cache.cache.get_tests_results(settings)


def create_cases_structure(settings):
    data_structure_created = []
    files_to_read = [join(settings["train_files"], f) for f in listdir(settings["train_files"]) if
                     f.lower().endswith(".csv")]
    for current_file in files_to_read:
        print("Reading and Analysing Train File:" + current_file)
        data_structure_created += analyse_structure(parse_structure(current_file, 4))
    return data_structure_created


def create_tags_structure(data_structure_created):
    tag_list = []
    for current_sentence in data_structure_created:
        for current_tag_in_list in current_sentence["taglist"]:
            if current_tag_in_list not in tag_list:
                tag_list.append("current_tag_in_list")
    return tag_list


def create_test_cases(settings):
    return analyse_structure(parse_structure(settings["test_file"], 3))


def save_if_necessary(settings, data_structure, test_cases):
    if not cache.cache.is_cache_information_available(settings) and settings['use_cache'] == True:
        cache.cache.save_cache(settings, data_structure, test_cases)


def create_data_structure(settings):
    print("Creating Test Cases")
    test_cases = create_test_cases(settings)
    print("Creating Main Data Structure")
    data_structure = create_cases_structure(settings)
    print("Saving Everything")
    save_if_necessary(settings, data_structure, test_cases)
    return cache.cache.get_data_structure_from_cache(settings)
