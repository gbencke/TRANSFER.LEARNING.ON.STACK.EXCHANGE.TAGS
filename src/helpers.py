from bs4 import BeautifulSoup


def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def remove_html_tags(text):
    parser = BeautifulSoup(text, "html.parser")
    [x.extract() for x in parser.findAll('code')]
    ret =  parser.get_text().lower()
    if '<code>' in ret:
        pass
    return ret

