import sys
from settings import settings
from nltk.data import path as nltk_data_path
from importlib import import_module


def show_usage():
    pass


def process_command(args):
    module_to_import = "tasks." + args[0]
    try:
        mod = import_module(module_to_import)
    except ImportError:
        print("Error, this command ({0}) was not found, tried to import: {1} ".format(args[0], module_to_import))
        return
    arguments_for_method_to_call = args[1:]
    method_to_call = "execute"
    method_pointer = getattr(mod, method_to_call)
    method_pointer(arguments_for_method_to_call)


if __name__ == '__main__':
    nltk_data_path.append(settings["nltk_location"])
    if len(sys.argv) == 1:
        show_usage()
    else:
        process_command(sys.argv[1:])

