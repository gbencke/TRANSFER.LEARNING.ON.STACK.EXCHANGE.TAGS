from prepare_data import get_tests_results
from submission.submission import generate_submission
from settings import settings


def execute(args):
    file_name = ''
    if len(args) < 1:
        print("Error you need to provide the filename...")
    else:
        file_name = args[0]
    test_cases_with_results = get_tests_results(settings)
    generate_submission(test_cases_with_results, file_name)
    print("Submission Generated, Good Luck!!")
