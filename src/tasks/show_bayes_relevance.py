from cache.metadata import create_metadata
from nltk import NaiveBayesClassifier
from settings import settings
import cache.cache


def execute(args):
    if len(args) < 1:
        print("You need to submit a list of tags to analyze...")
    md = create_metadata(settings)
    train_cases = cache.cache.get_cases_from_cache(md)
    for tag in args:
        to_train = []
        for case in train_cases:
            list_features = {}
            try:
                for noun in case['nouns']:
                    list_features[noun] = True
                to_train.append((list_features, True if tag in case['taglist'] else False))
            except:
                pass
        classifier = NaiveBayesClassifier.train(to_train)
        print("Structure for:{}".format(tag))
        classifier.show_most_informative_features()
