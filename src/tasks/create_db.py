from settings import settings
from prepare_data import get_data_structure


def execute(args):
    train_cases, tag_list, test_cases = get_data_structure(settings)
    print("Created: {} train cases, {} distinct tags, {} test cases ".format(len(train_cases), len(tag_list),
                                                                             len(test_cases)))
