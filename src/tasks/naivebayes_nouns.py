import os.path
import time
import psutil
from cache.cache import save_accuracy, save_test_hit, create_metadata, reset_results
from prepare_data import get_data_structure
from settings import settings
from nltk import NaiveBayesClassifier, classify
from random import shuffle
from copy import deepcopy
from multiprocessing import Process


def process_tag(settings, tag, shuffled_list, test_cases, tag_list_size, tag_counter):
    md = create_metadata(settings)
    to_train = []
    for case in shuffled_list:
        list_features = {}
        for noun in case['nouns']:
            list_features[noun] = True
        to_train.append((list_features, True if tag[0] in case['taglist'] else False))
    cut_position = int(len(to_train) * 0.7)
    train_set = to_train[:cut_position]
    test_set = to_train[cut_position + 1:]
    classifier = NaiveBayesClassifier.train(train_set)
    accuracy = classify.accuracy(classifier, test_set)
    if accuracy < settings['mininum_accuracy']:
        return None
    save_accuracy(settings, "NaiveBayesClassifierNounsOnly", tag[0], accuracy)
    print("Accuracy: ({}/{}), Tag {}:{}".format(tag_counter, tag_list_size, tag, accuracy))
    classifier = NaiveBayesClassifier.train(to_train)
    for test in test_cases:
        list_features = {}
        for noun in test['nouns']:
            list_features[noun] = True
        classifier_result = classifier.classify(list_features)
        if classifier_result:
            save_test_hit(md, test['unique_id'], tag[0], "NaiveBayesClassifierNounsOnly")


def wait_for_process(created_process):
    process_alive = 0
    for p in created_process:
        try:
            pstatus = psutil.Process(p.pid)
            if pstatus.status() == psutil.STATUS_ZOMBIE:
                p.join()
                continue
        except:
            pass
        if os.path.exists("/proc/{}".format(p.pid)):
            process_alive += 1
    return process_alive


def start_process(settings, tag_to_use):
    created_process = []
    train_cases, tag_list, test_cases = get_data_structure(settings)
    if not tag_to_use is None:
        tag_list = [tag_to_use]
    shuffled_list = deepcopy(train_cases)
    shuffle(shuffled_list)
    tag_counter = 0
    tag_list_size = len(tag_list)
    for tag in tag_list:
        while True:
            if wait_for_process(created_process) > settings['max_process']:
                time.sleep(1)
            else:
                break
        print("Processing Tag:{}, {}".format(tag, tag_counter))
        p = Process(target=process_tag, args=(settings, tag, shuffled_list, test_cases, tag_list_size, tag_counter))
        created_process.append(p)
        p.start()
        tag_counter += 1
    for p in created_process:
        p.join()


def execute(args):
    TagToUse = None
    if len(args) > 0:
        TagToUse = args[0]
    start_process(settings, TagToUse)
