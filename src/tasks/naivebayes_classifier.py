from io import StringIO
from prepare_data import get_data_structure
from settings import settings
from nltk import NaiveBayesClassifier
import sys


def process_tag(settings_for_processing, tag, file_to_save, train_cases, test_cases, Mode='nouns'):
    sys.stdout = mystdout = StringIO()
    number_of_hits = 0
    to_train = []
    for case in train_cases:
        list_features = {}
        if Mode == 'nouns' or Mode == 'all':
            for noun in case['nouns']:
                list_features[noun] = True
        if Mode == 'keywords' or Mode == 'all':
            for keyword in case['keywords']:
                if keyword[1] >= settings_for_processing['keyword_relevance']:
                    list_features[keyword[0]] = True
        if not Mode in ('nouns', 'keywords','all'):
            raise Exception('Only nouns or keywords mode are supported')
        to_train.append((list_features, True if tag in case['taglist'] else False))

    classifier = NaiveBayesClassifier.train(to_train)
    print('-----------------------------------------')
    classifier.show_most_informative_features(1000)
    print('-----------------------------------------')

    for test in test_cases:
        list_features = {}
        if Mode == 'nouns' or Mode == 'all':
            for noun in test['nouns']:
                list_features[noun] = True
        if Mode == 'keywords' or Mode == 'all':
            for keyword in test['keywords']:
                if keyword[1] >= settings_for_processing['keyword_relevance']:
                    list_features[keyword[0]] = True
        classifier_result = classifier.classify(list_features)
        if classifier_result:
            prob_dict = classifier.prob_classify(list_features)._prob_dict
            if prob_dict[True] >= 0:
                print('Test ({}/{}) hit:'.format(test['unique_id'], test['file']))
                print('Title:{}'.format(test['title']))
                if Mode == 'nouns' or Mode == 'all':
                    print('nouns:{}'.format(test['nouns']))
                if Mode == 'keywords' or Mode == 'all':
                    print('keywords:{}'.format([x for x in test['keywords'] if x[1] > settings_for_processing['keyword_relevance'] ]))
                print(classifier.prob_classify(list_features)._prob_dict)
                print('-----------------------------------------')
                number_of_hits += 1
    print('Number Of Hits:{}'.format(number_of_hits))
    print('-----------------------------------------')
    f = open("{}.{}.txt".format(file_to_save, tag), "w", encoding="utf-8")
    f.write(mystdout.getvalue())
    f.close()


def execute(args):
    TagToUse = None
    Mode = 'nouns'
    if len(args) > 0:
        TagToUse = args[0]
    if len(args) > 1:
        Mode = args[1]
    if len(args) > 2:
        settings['keyword_relevance'] = float(args[2])
    else:
        settings['keyword_relevance'] = 1
    train_cases, tag_list, test_cases = get_data_structure(settings)
    process_tag(settings, TagToUse, '../classifiers/classifier', train_cases, test_cases, Mode)
