from cache.metadata import create_metadata
from settings import settings
from cache.cache import get_cases_from_cache
import RAKE


def execute(args):
    if len(args) < 1:
        print("Error, you must provide the filename with stoplists.txt")
    md = create_metadata(settings)
    train_cases = get_cases_from_cache(md)
    for case in train_cases[:10]:
        rake = RAKE.Rake(args[0])
        print(case['id'])
        print(rake.run(case['stripped_text']))
