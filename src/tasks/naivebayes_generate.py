import os.path
import time
import psutil
from cache.cache import save_test_hit, create_metadata
from prepare_data import get_data_structure
from settings import settings
from nltk import NaiveBayesClassifier


def process_tag(settings_for_processing, tag, train_cases, test_cases, precision, Mode='nouns'):
    print("Processing tag:{}".format(tag))
    md = create_metadata(settings)
    to_train = []
    for case in train_cases:
        list_features = {}
        if Mode == 'nouns' or Mode == 'all':
            for noun in case['nouns']:
                list_features[noun] = True
        if Mode == 'keywords' or Mode == 'all':
            for keyword in case['keywords']:
                if keyword[1] >= settings_for_processing['keyword_relevance']:
                    list_features[keyword[0]] = True
        if Mode not in ('nouns', 'keywords', 'all'):
            raise Exception('Only nouns or keywords mode are supported')
        to_train.append((list_features, True if tag[0] in case['taglist'] else False))
    classifier = NaiveBayesClassifier.train(to_train)
    for test in test_cases:
        list_features = {}
        if Mode == 'nouns' or Mode == 'all':
            for noun in test['nouns']:
                list_features[noun] = True
        if Mode == 'keywords' or Mode == 'all':
            for keyword in test['keywords']:
                if keyword[1] >= settings_for_processing['keyword_relevance']:
                    list_features[keyword[0]] = True
        classifier_result = classifier.classify(list_features)
        if classifier_result:
            prob_dict = classifier.prob_classify(list_features)._prob_dict
            if prob_dict[True] >= ((10 ** precision) * -1):
                save_test_hit(md, test['unique_id'], tag[0],
                              "NaiveBayes.{}.{}".format(settings['keyword_relevance'], precision))


def wait_for_process(created_process):
    process_alive = 0
    for p in created_process:
        try:
            pstatus = psutil.Process(p)
            if pstatus.status() == psutil.STATUS_ZOMBIE:
                os.waitpid(p, os.WNOHANG)
                continue
        except Exception as ex:
            pass
        if os.path.exists("/proc/{}".format(p)):
            process_alive += 1
    return process_alive


def execute(args):
    created_process = []
    precision = -10
    Mode = 'nouns'
    if len(args) > 0:
        Mode = args[0]
    if len(args) > 1:
        settings['keyword_relevance'] = float(args[1])
    else:
        settings['keyword_relevance'] = 1
    if len(args) > 2:
        precision = int(args[2])
    train_cases, tag_list, test_cases = get_data_structure(settings)
    for tag in tag_list:
        while True:
            if wait_for_process(created_process) > settings['max_process']:
                time.sleep(1)
            else:
                break
        created_pid = os.fork()
        if created_pid == 0:
            process_tag(settings, tag, train_cases, test_cases, precision, Mode)
            exit()
        else:
            created_process.append(created_pid)
