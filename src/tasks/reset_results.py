from settings import settings
from cache.cache import reset_results


def execute(args):
    reset_results(settings)
    print("Results Reset")
