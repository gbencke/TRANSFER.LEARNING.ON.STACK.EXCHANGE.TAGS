import _pickle
import RAKE
from settings import settings
from prepare_data import get_data_structure


def execute(args):
    train_cases, tag_list, test_cases = get_data_structure(settings)
    print("Created: {} train cases, {} distinct tags, {} test cases ".format(len(train_cases), len(tag_list),
                                                                             len(test_cases)))
    folder_to_store_pickle_file = settings['pickling_folder']
    if not folder_to_store_pickle_file.endswith('/'):
        folder_to_store_pickle_file += '/'

    rake_object = RAKE.Rake(settings['rake_stop_words'])

    print("Create keywords for train cases")
    for case in train_cases:
        print("{}-{} keywords created".format(case['file'],case['id']))
        case['keywords'] = rake_object.run(case['stripped_text'])
    print("Create keywords for test cases")
    for case in test_cases:
        print("{}-{} keywords created".format(case['file'], case['id']))
        case['keywords'] = rake_object.run(case['stripped_text'])

    _pickle.dump(train_cases, open(folder_to_store_pickle_file + "train.pickle", 'wb'))
    _pickle.dump(test_cases, open(folder_to_store_pickle_file + "test.pickle", 'wb'))
    _pickle.dump(tag_list, open(folder_to_store_pickle_file + "tag_list.pickle", 'wb'))
