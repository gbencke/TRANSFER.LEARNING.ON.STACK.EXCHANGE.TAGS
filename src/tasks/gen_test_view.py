from cache.pickle import *
from cache.metadata import create_metadata
from settings import settings
from cache.cache import get_test_cases_from_cache
from visualization.data_visualization import create_cases_view


def execute(args):
    if len(args) < 1:
        print("Error, you must provide the filename to save the html")
    if pickle_file_is_available("test.pickle"):
        test_cases = read_pickle("test.pickle")
    else:
        md = create_metadata(settings)
        test_cases = get_test_cases_from_cache(md)
    current_file = 0
    while True:
        current_test_case = test_cases[:1000]
        test_cases = test_cases[1000:]
        create_cases_view('{}{}.html'.format(args[0], str(current_file).zfill(3)), current_test_case)
        current_file += 1
        if len(current_test_case) < 1000:
            break
