from prepare_data import get_data_structure
from settings import settings


def execute(args):
    train_cases, tag_list, test_cases = get_data_structure(settings)
    print('Train cases:{} cases'.format(len(train_cases)))
    print('Tag List:{} tags'.format(len(tag_list)))
    print('Test cases:{} cases'.format(len(test_cases)))



