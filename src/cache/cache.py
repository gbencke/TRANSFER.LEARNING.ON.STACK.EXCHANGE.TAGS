from cache.metadata import create_metadata
from sqlalchemy.sql import select, func, desc
from sqlalchemy.exc import ProgrammingError


def is_information_available(settings):
    md = create_metadata(settings)
    conn = md.bind.connect()
    s = select([md.tables['cases']]).limit(1)
    cases_returned_from_sql = conn.execute(s)
    return cases_returned_from_sql.rowcount > 0


def is_cache_information_available(settings):
    try:
        return is_information_available(settings)
    except ProgrammingError as ex:
        return False


def get_cases_from_cache(md):
    cases = []
    conn = md.bind.connect()
    s = select([md.tables['cases']])
    cases_returned_from_sql = conn.execute(s)
    rowcount_returned = cases_returned_from_sql.rowcount
    counter = 0
    for current_case in cases_returned_from_sql:
        if (counter % 1000) == 0:
            print("Read Train Case {} of {}".format(counter, rowcount_returned))
        case_to_append = {
            "unique_id": current_case[0],
            "file": current_case[1],
            "id": current_case[2],
            "title": current_case[3],
            "content": current_case[4],
            "stripped_text": current_case[5]}

        s_tags = select([md.tables['tags']]).where(md.tables['tags'].c.case_id == case_to_append["unique_id"])
        tags_returned_from_sql = conn.execute(s_tags)
        list_tags_returned = []
        for current_tag in tags_returned_from_sql:
            list_tags_returned.append(current_tag[2])
        case_to_append["taglist"] = list_tags_returned

        s_nouns = select([md.tables['nouns']]).where(md.tables['nouns'].c.case_id == case_to_append["unique_id"])
        nouns_returned_from_sql = conn.execute(s_nouns)
        list_nouns_returned = []
        for current_noun in nouns_returned_from_sql:
            list_nouns_returned.append(current_noun[2])
        case_to_append["nouns"] = list_nouns_returned
        cases.append(case_to_append)
        counter += 1
    return cases


def get_test_cases_from_cache(md):
    test_cases = []
    conn = md.bind.connect()
    s = select([md.tables['cases_test']])
    cases_returned_from_sql = conn.execute(s)
    rowcount_returned = cases_returned_from_sql.rowcount
    counter = 0
    for current_case in cases_returned_from_sql:
        if (counter % 1000) == 0:
            print("Read Test Case {} of {}".format(counter, rowcount_returned))
        case_to_append = {
            "unique_id": current_case[0],
            "file": current_case[1],
            "id": current_case[2],
            "title": current_case[3],
            "content": current_case[4],
            "stripped_text": current_case[5]}

        s_tags = select([md.tables['tags_test']]).where(md.tables['tags_test'].c.case_id == case_to_append["unique_id"])
        tags_returned_from_sql = conn.execute(s_tags)
        list_tags_returned = []
        for current_tag in tags_returned_from_sql:
            list_tags_returned.append(current_tag[2])
        case_to_append["taglist"] = list_tags_returned

        s_nouns = select([md.tables['nouns_test']]).where(
            md.tables['nouns_test'].c.case_id == case_to_append["unique_id"])
        nouns_returned_from_sql = conn.execute(s_nouns)
        list_nouns_returned = []
        for current_noun in nouns_returned_from_sql:
            list_nouns_returned.append(current_noun[2])
        case_to_append["nouns"] = list_nouns_returned
        test_cases.append(case_to_append)
        counter += 1
    return test_cases


def get_tags_from_cache(md):
    all_tags = []
    conn = md.bind.connect()
    s_tags_to_return = select([md.tables['tags'].c.tag, func.count(md.tables['tags'].c.tag)]). \
        group_by(md.tables['tags'].c.tag).having(func.count(md.tables['tags'].c.tag) > 0). \
        order_by(desc(func.count(md.tables['tags'].c.tag)))
    tags_returned_from_sql = conn.execute(s_tags_to_return)
    for current_tag in tags_returned_from_sql:
        all_tags.append(current_tag)
    return all_tags


def get_data_structure_from_cache(settings):
    md = create_metadata(settings)
    return get_cases_from_cache(md), get_tags_from_cache(md), get_test_cases_from_cache(md)


def get_tests_results(settings):
    md = create_metadata(settings)
    return get_test_cases_from_cache(md)


def save_cache_train_data(md, conn, data_structure):
    counter = 0
    last_counter = 0
    for case in data_structure:
        current_insert = md.tables['cases'].insert().values(
            id=case['id'],
            file=case['file'],
            title=case['title'],
            content=case['content'],
            stripped_text=case['stripped_text']
        )
        created_case_id = conn.execute(current_insert).inserted_primary_key[0]
        for current_tag in case["taglist"]:
            current_insert_tag = md.tables['tags'].insert().values(
                case_id=created_case_id,
                tag=current_tag)
            conn.execute(current_insert_tag)
        for current_noun in case["nouns"]:
            current_insert_noun = md.tables['nouns'].insert().values(
                case_id=created_case_id,
                noun=current_noun)
            conn.execute(current_insert_noun)
        new_counter = int((counter * 10000) / len(data_structure))
        if new_counter != last_counter:
            print("Saving Train Case {}% ".format(new_counter / 100))
            last_counter = new_counter
        counter += 1


def save_cache_test_data(md, conn, test_cases):
    counter = 0
    last_counter = 0
    for case in test_cases:
        current_insert = md.tables['cases_test'].insert().values(
            id=case['id'],
            file=case['file'],
            title=case['title'],
            content=case['content'],
            stripped_text=case['stripped_text']
        )
        created_case_id = conn.execute(current_insert).inserted_primary_key[0]
        for current_noun in case["nouns"]:
            current_insert_noun = md.tables['nouns_test'].insert().values(
                case_id=created_case_id,
                noun=current_noun)
            conn.execute(current_insert_noun)
        new_counter = int((counter * 10000) / len(test_cases))
        if new_counter != last_counter:
            print("Saving Test Case {}% ".format(new_counter / 100))
            last_counter = new_counter
        counter += 1


def save_cache(settings, data_structure, test_cases):
    md = create_metadata(settings)
    md.create_all()
    conn = md.bind.connect()
    save_cache_train_data(md, conn, data_structure)
    save_cache_test_data(md, conn, test_cases)


def save_accuracy(settings, model, tag_name, accuracy):
    pass


def save_test_hit(md, test_id, tag, model_used):
    conn = md.bind.connect()
    current_insert_noun = md.tables['tags_test'].insert().values(
        case_id=test_id,
        tag=tag,
        model_used=model_used)
    conn.execute(current_insert_noun)


def reset_results(settings):
    md = create_metadata(settings)
    conn = md.bind.connect()
    current_delete_tags = md.tables['tags_test'].delete()
    conn.execute(current_delete_tags)


def reset_db(settings):
    md = create_metadata(settings)
    md.drop_all()
