from sqlalchemy import MetaData, Table, Column, String, Integer, ForeignKey


def create_engine_sqlite3(settings):
    return settings['sqlalchemy_connecion_string']


def create_metadata(settings):
    md = MetaData(create_engine_sqlite3(settings))
    Table("cases", md,
          Column('unique_id', Integer, primary_key=True, autoincrement=True),
          Column("file", String, nullable=False),
          Column("id", Integer, nullable=False),
          Column("title", String, nullable=False),
          Column("content", String, nullable=False),
          Column("stripped_text", String, nullable=False))
    Table("nouns", md,
          Column('unique_id', Integer, primary_key=True, autoincrement=True),
          Column("case_id", Integer, ForeignKey("cases.unique_id"), nullable=False, index=True),
          Column("noun", String, nullable=False))
    Table("tags", md,
          Column('unique_id', Integer, primary_key=True, autoincrement=True),
          Column("case_id", Integer, ForeignKey("cases.unique_id"), nullable=False, index=True),
          Column("tag", String, nullable=False))
    Table("cases_test", md,
          Column('unique_id', Integer, primary_key=True, autoincrement=True),
          Column("file", String, nullable=False),
          Column("id", Integer, nullable=False),
          Column("title", String, nullable=False),
          Column("content", String, nullable=False),
          Column("stripped_text", String, nullable=False))
    Table("nouns_test", md,
          Column('unique_id', Integer, primary_key=True, autoincrement=True),
          Column("case_id", Integer, ForeignKey("cases.unique_id"), nullable=False, index=True),
          Column("noun", String, nullable=False))
    Table("tags_test", md,
          Column('unique_id', Integer, primary_key=True, autoincrement=True),
          Column("case_id", Integer, ForeignKey("cases.unique_id"), nullable=False, index=True),
          Column("tag", String, nullable=False),
          Column("model_used", String, nullable=False))
    return md
