import os
import _pickle
from pathlib import Path
from settings import settings


def read_pickle(file_to_read):
    return _pickle.load(open(get_pickle_file(file_to_read), 'rb'))


def get_pickle_file(file_to_check):
    cwd = os.getcwd()
    folder_to_store_pickle_file = settings['pickling_folder']
    if not folder_to_store_pickle_file.endswith('/'):
        folder_to_store_pickle_file += '/'
    return cwd + '/' + folder_to_store_pickle_file + file_to_check


def pickle_file_is_available(file_to_check):
    try:
        pickle_file = Path(get_pickle_file(file_to_check))
        if pickle_file.is_file():
            return True
        else:
            return False
    except:
        return False
