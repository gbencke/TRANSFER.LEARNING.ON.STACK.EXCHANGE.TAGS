from operator import itemgetter


def generate_submission(test_cases, file_name):
    test_cases_sorted = sorted(test_cases, key=itemgetter('id'))
    submission_file = open("../results/{}.txt".format(file_name), 'w')
    submission_file.write('"id","tags"\n')
    for test in test_cases_sorted:
        submission_file.write('"{}","{}"\n'.format(test['id'], ("physics " + " ".join(test["taglist"])).strip(" ")))
    submission_file.close()
