settings = {
    "use_cache": True,
    "reset_database": False,
    "database_file": '../data/stackoverflow_tags.db',
    "sqlalchemy_connecion_string": 'postgresql://stackoverflow_tags:2253423@172.17.0.2:5432/stackoverflow_tags',
    "nltk_location": "/home/gbencke/.nltk_data",
    "test_file": '../data/test/test.csv',
    "train_files": '../data/train',
    "tokens_per_line": 4,
    "max_process": 2,
    "mininum_accuracy": 0,
    "pickling_folder": "../pickling",
    "rake_stop_words": "../data/SmartStoplist.txt"
}


