#!/bin/bash

sudo apt-get update
sudo apt-get install git tmux python3 python3-pip postgresql postgresql-contrib htop libpq-dev
locale-gen en_US en_US.UTF-8 hu_HU hu_HU.UTF-8; dpkg-reconfigure locales


